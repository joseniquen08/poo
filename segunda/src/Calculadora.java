public class Calculadora {
    private Double primero;
    private Double segundo;
    private Double resultado;
    public void sumar(){
        resultado = primero+segundo;
    }
    public void multiplicar(){
        resultado = primero*segundo;
    }

    public static void main(String[] args) {
        Calculadora a = new Calculadora();
        a.primero=5.0;
        a.segundo=7.3;
        a.sumar();
        System.out.println(a.resultado);
        Calculadora b = new Calculadora();
        b.primero=4.5;
        b.segundo=8.0;
        b.multiplicar();
        System.out.println(b.resultado);
    }
}